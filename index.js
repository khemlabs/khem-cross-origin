let origins = [];
let allow = false;

const parseURL = url => (url.substr(url.length - 1) == '/' ? url : url + '/');

const parse = function(req, res, next) {
	if (req.headers && req.headers.origin && origins.length) {
		// Only allow the cross origin if it is in white list
		const url = parseURL(req.headers.origin);
		if (allow || origins.indexOf(url) >= 0) {
			res.setHeader('Access-Control-Allow-Origin', req.headers.origin);
			res.setHeader('Access-Control-Allow-Credentials', true);
			res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');
			res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,content-type,Authorization');
		} else {
			// Log warning, domain not allowed
		}
		return next();
	} else {
		return next();
	}
};

// Add Access-Control-Allow-Origin headers
module.exports = ALLOW_ORIGIN => {
	allow = ALLOW_ORIGIN.length && ALLOW_ORIGIN.indexOf('*') >= 0;
	origins = !allow && ALLOW_ORIGIN.length ? ALLOW_ORIGIN.map(origin => parseURL(origin)) : [];
	return parse;
};
